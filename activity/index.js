/*
	MINI ACTIVITY

	1. using the es6 update, get the cube of 8.
	2. Print the result on the console with the message: "The cube of 8 is + result"
	3. use the template literals in printing out the message
*/

const cube8 = 8**3;
console.log(`The cube of 8 is ${cube8}`);



/*
	detructure the address array
	print the values in the console:
		I live in 258 Washington Avenue, California, 99011
*/
const address = ["258", "Washington Ave NW", "California", "99011"]
const [streetNumber, street, country, zip] = address;

console.log(`I live in ${streetNumber} ${street}, ${country}, ${zip}`);





const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20ft 3 in"
}
// object destructuring
const {name, species, weight, measurement} = animal;
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}`);


/*
	MINI ACTIVITY

	1 loop through the numbes using forEach using arrow function
	2 print the numbes in the console
	3 use the .reduce operator on the numbers array
	4 assign the result on a variable
	5 print the variable on the console
*/

let numbers = [1, 2, 3, 4, 5];
numbers.forEach((number) => console.log(number));
let reduceNumber = numbers.reduce((x,y) => x+y);
console.log(reduceNumber);



/*
	MINI ACTIVITY

	1. creat a DOG  class 
	2. inside of the class DOG, have a name, age, and breed
	3. Instantiate a new dog class and print in the console
	4. send the screenshot of the output on hangouts
*/


class dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
const newDog = new dog("Bantay", 2, "askal");
console.log(newDog);