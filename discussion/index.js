/*
	es6 updates

	EXPONENT OPERATOR

*/

// old:
console.log("REsult of old");
const oldNum = Math.pow(8,2);
console.log(oldNum);

// new
console.log("REsult of es6");
const newNum = 8**2;
console.log(newNum);

/*
	TEMPLATE LITERALS

	Allows us to write strings without the concatenate operator (+)
*/

let studentName = "Roland";

// pre-template literal string
console.log("Hello "+studentName+"! Welcome to programming!");

// template lietral string
console.log(`Hello ${studentName}! Welcome to programming!`);

// multi line template literal
const message = `
${studentName} attended a math competition.
He won it by soling the problem 8 ** 2 with the solution of ${newNum}.
`
console.log(message);

/*
	-template literals allow us to write strings with embedded js expressions
	-"${}" are used to include js expression in strings using template literals
*/

const interestRate = .1;
const principal = 1000;

console.log(`The interest rate on your savings acct is: ${principal*interestRate}`);


/*
	ARRAY DESCTRUCTING

	-allows us to unpack elements in array into distinict varialbes
	-allows us to name array elements with variables instead of using index numbers

		let/const [variableName, variableName, variableName] = array;

*/

console.log("");

console.log("Array destructuring")
const fullName = ["Jeru", "Nebur", "Palma"];

// pre array destructuring
console.log("Pre-Array destructuring")
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}`);


console.log("");


// Array destructuring
console.log("Array destructuring")
const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);
console.log(`Hello ${firstName} ${middleName} ${lastName}`);



/*
	OBJECT DESTRUCTURING

	-allows us to unpack properties on objects into diestinct values
	-shortens the syntax for accessing properties from objects

		let/const {propertyName, propertyName} = object;
*/

console.log("object destructuring")

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}

// pre object destructuring
console.log("pre-object destructuring")
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName} `)

// object destructuring
const {givenName, maidenName, familyName} = person;
console.log("object destructuring")
console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello ${givenName} ${middleName} ${familyName}`);




/*ARROW FUNCTIONS

	-compact alternative syntax to traditional functions
	-useful for creating code snippets  where creating functions will not be reused in any other portions of the code
	-adheres to the DRY principle (do not repeat yourself) pirnciple where there is no longer a need to create a new function and think of a name for functions that will only by used in certain code snippets
*/


const hello = () =>{
	return "Good morning"
}

/*
	SYNTAX
		let/const variableName = (parameterA, parameterB) => {
		console.log();
		}
*/

const printFullName = (firstN, middleI, lastN) => {
	console.log(`${firstN} ${middleI} ${lastN}`)
}
printFullName("John", "D", "Smith");



const students = ["John", "Jane", "Smith"];

// arrow function with loops

students.forEach((student) => {
	console.log(`${students} is a student`);
})


/*
	IMPLICIT RETURN STATEMENT

	-there are instance when you cna omit the RETURN  statement
	-this works because even without the return statement, js adds for it for the result of the function
*/



// old or pre arrow function
const add = (x, y) => {
	return x+y;
}
let total = add(1,2);
console.log(total);


//  arrow function
const subtract = (x,y) => x-y
let difference = subtract(3,1);
console.log(difference);




/*
	DEFAULT ARGUMENT
	provides a default argument if non is provided when the function is invoked
*/
console.log("default argument value");
const greet = (name = "User") => {
	return	`Good morning, ${name}`
}
console.log(greet());
console.log(greet("Goku"));


/*
	CLASS-BASED OBJECT BLUEPRINTS

	allows creation or instantiation of objects as blueprints

	CREATING A CLASS:

	the constructor function is a special method of class for creating or initializing an object for that class.
	-the THIS keyword referes to the properties initialized from the inside the class

	SYNTAX:
		class className{
			constructor(objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
		}


*/

class Car{
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}
console.log("");


/*

	-the NEW operator creates or instatiaties a new object with the given argument as the value
	-no arguments provided will create an object without any values assigned to its properties


	let/const variable name = new className();



*/

console.log("Class Blueprints");
let myCar = new Car();
console.log(myCar);
console.log("");

// assigning properties afte creation or instantitation of an object
myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;
console.log(myCar);


// instantiate a new object

const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);